GO?=go
GOPATH?=$(shell go env GOPATH)
GOPACKAGES=$(shell go list ./... | grep -v generated)

GOFLAGS   = -mod=vendor

export GOFLAGS   := $(GOFLAGS)
export GOPRIVATE := $(GOPRIVATE)
export GOPROXY   := $(GOPROXY)
export GOBIN     := $(PWD)/bin

help:
	@echo "help             - show this help"
	@echo "build            - build application from sources"
	@echo "clean            - remove build artifacts"
	@echo "fmt              - format application sources"
	@echo "check            - check code style"
	@echo "imports-install  - install goimport to local ./bin"
	@echo "imports          - format imports"
	@echo "deps-tidy        - go mod tidy && go mod vendor"

build: clean fmt check
	GOPATH=$(GOPATH) $(GO) build -o bin/coinconv ./cmd/service

clean:
	GOPATH=$(GOPATH) $(GO) clean
	rm -rf bin/

fmt:
	GOPATH=$(GOPATH) $(GO) fmt ${GOPACKAGES}

check:
	GOPATH=$(GOPATH) $(GO) vet $(GOPACKAGES)

test: clean
	$(GO) test $(GOPACKAGES)

coverage: clean
	$(GO) test -v -cover $(GOPACKAGES)

imports-install: toolset

imports:
	@./bin/goimports -ungroup -local coinconv -w ./internal ./cmd

deps-tidy:
	@go mod tidy
	@if [[ "`go env GOFLAGS`" =~ -mod=vendor ]]; then go mod vendor; fi

TOOLFLAGS = -mod=
toolset:
	@( \
		GOFLAGS=$(TOOLFLAGS); \
		cd tools; \
		go mod download; \
		go generate tools.go; \
	)
