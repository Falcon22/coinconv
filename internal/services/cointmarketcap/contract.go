package cointmarketcap

import (
	"context"

	"coinconv/internal/services/cointmarketcap/client"
)

type CoinMarketCap interface {
	GetCurrencyMap(context.Context) (map[string]interface{}, error)
	GetCurrencyQuote(context.Context, string, string) (float64, error)
}

type Client interface {
	CryptocurrencyMapV1(context.Context) ([]client.CryptocurrencyData, error)
	CryptocurrencyQuotesV1(context.Context, string, string) (map[string]client.QuotesData, error)
}
