package cointmarketcap

import (
	"context"
	"errors"
	"fmt"
)

type service struct {
	client Client
}

func New(client Client) *service {
	return &service{
		client: client,
	}
}

func (s *service) GetCurrencyMap(ctx context.Context) (map[string]interface{}, error) {
	result := make(map[string]interface{})
	currencyMap, err := s.client.CryptocurrencyMapV1(ctx)
	if err != nil {
		return result, err
	}

	for _, data := range currencyMap {
		result[data.Symbol] = struct{}{}
	}
	return result, nil
}

func (s *service) GetCurrencyQuote(ctx context.Context, symbol, convert string) (float64, error) {
	currencyQuotes, err := s.client.CryptocurrencyQuotesV1(ctx, symbol, convert)
	if err != nil {
		return 0, err
	}

	symbolValue, hasSymbol := currencyQuotes[symbol]
	if !hasSymbol {
		return 0, errors.New(fmt.Sprintf("cryptocurrency %s is not found", symbol))
	}

	quoteValue, hasValue := symbolValue.Quotes[convert]
	if !hasValue {
		return 0, errors.New(fmt.Sprintf("currency %s is not found", convert))
	}

	return quoteValue.Price, nil
}
