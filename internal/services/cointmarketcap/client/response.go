package client

type CryptocurrencyMapV1Response struct {
	Status Status               `json:"status"`
	Data   []CryptocurrencyData `json:"data"`
}

type CryptocurrencyQuotesDataV1Response struct {
	Status Status                `json:"status"`
	Data   map[string]QuotesData `json:"data"`
}

type Status struct {
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
}

type CryptocurrencyData struct {
	Symbol string `json:"symbol"`
}

type QuotesData struct {
	Symbol string           `json:"symbol"`
	Quotes map[string]Quote `json:"quote"`
}

type Quote struct {
	Price float64 `json:"price"`
}
