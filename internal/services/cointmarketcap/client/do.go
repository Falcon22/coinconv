package client

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/kamilsk/retry/v4"
	"github.com/kamilsk/retry/v4/backoff"
	"github.com/kamilsk/retry/v4/strategy"
	"github.com/pkg/errors"
	"go.octolab.org/io"
)

var (
	ErrEmptyResponse = "empty response"
)

func (client *client) do(ctx context.Context, req *http.Request) (resp *http.Response, err error) {
	var buf *bytes.Buffer
	reset := func() {}
	if req.Body != nil {
		buf = bytes.NewBuffer(make([]byte, 0, req.ContentLength))
		req.Body = io.TeeReadCloser(req.Body, buf)
		reset = func() { req.Body = ioutil.NopCloser(bytes.NewReader(buf.Bytes())) }
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-CMC_PRO_API_KEY", client.apikey)
	what := func(attempt uint) error {
		if attempt > 0 {
			reset()
		}
		resp, err = client.transport.Do(req.WithContext(ctx))
		if resp == nil && err == nil {
			err = errors.New(ErrEmptyResponse)
		}
		return err
	}
	how := retry.How{
		strategy.Limit(client.attemptLimit),
		strategy.Backoff(backoff.Linear(20 * time.Millisecond)),
	}

	return resp, retry.Try(ctx, what, how...)
}
