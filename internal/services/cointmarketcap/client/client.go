package client

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"

	"github.com/pkg/errors"
)

const (
	cryptocurrencyMapPath    = "/v1/cryptocurrency/map"
	cryptocyrrencyQuotesPath = "/v1/cryptocurrency/quotes/latest"
)

type client struct {
	transport    *http.Client
	endpoint     *url.URL
	attemptLimit uint

	apikey string
}

// New возвращает новый экземпляр клиента для поиска
// или ошибку, если его не получается сконфигурировать.
func New(host, apikey string, options ...Option) (*client, error) {
	endpoint, err := url.Parse(host)
	if err != nil {
		return nil, errors.Wrap(err, "client: invalid host")
	}
	if endpoint.Host == "" {
		return nil, errors.New("client: endpoint host is empty")
	}
	client := &client{
		endpoint:     endpoint,
		attemptLimit: 3,
	}
	for i, configure := range options {
		if err := configure(client); err != nil {
			return nil, errors.Wrapf(err, "client: invalid option %d", i)
		}
	}
	if client.transport == nil {
		return nil, errors.New("client: please provide custom transport or use default")
	}
	if len(apikey) == 0 {
		return nil, errors.New("client: please provide apikey")
	}
	client.apikey = apikey

	return client, nil
}

func (c *client) CryptocurrencyMapV1(ctx context.Context) ([]CryptocurrencyData, error) {
	copied := *(c.endpoint)
	copied.Path = path.Join(c.endpoint.Path, cryptocurrencyMapPath)
	req, err := http.NewRequest(http.MethodGet, copied.String(), nil)
	if err != nil {
		return []CryptocurrencyData{}, errors.Wrap(err, "can't create cryptocurrencyMapV1 http request")
	}

	resp, err := c.do(ctx, req)
	if err != nil {
		return []CryptocurrencyData{}, errors.Wrap(err, "can't get cryptocurrencyMap")
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return []CryptocurrencyData{}, errors.New(fmt.Sprintf("error from coinMarketCap, status = %s", resp.Status))
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []CryptocurrencyData{}, err
	}

	var result CryptocurrencyMapV1Response
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		return nil, err
	}

	if result.Status.ErrorCode != 0 {
		return nil, errors.New(fmt.Sprintf("error from coinMarketCap, error message: %s", result.Status.ErrorMessage))
	}

	return result.Data, nil
}

func (c *client) CryptocurrencyQuotesV1(ctx context.Context, symbol, convert string) (map[string]QuotesData, error) {
	copied := *(c.endpoint)
	copied.Path = path.Join(c.endpoint.Path, cryptocyrrencyQuotesPath)
	req, err := http.NewRequest(http.MethodGet, copied.String(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "can't create cryptocurrencyMapV1 http request")
	}
	q := url.Values{}
	q.Add("symbol", symbol)
	q.Add("convert", convert)
	req.URL.RawQuery = q.Encode()

	resp, err := c.do(ctx, req)
	if err != nil {
		return nil, errors.Wrap(err, "can't get cryptocurrencyMap")
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("error from coinMarketCap, status = %s", resp.Status))
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result CryptocurrencyQuotesDataV1Response
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		return nil, err
	}

	if result.Status.ErrorCode != 0 {
		return nil, errors.New(fmt.Sprintf("error from coinMarketCap, error message: %s", result.Status.ErrorMessage))
	}

	return result.Data, nil
}
