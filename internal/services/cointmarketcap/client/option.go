package client

import (
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/pkg/errors"
)

const (
	defaultTimeout      = 10 * time.Second
	defaultMaxIdleConns = 50
)

// Option задаёт настройку клиенту.
type Option func(*client) error

// WithCustomTransport устанавливает специализированный транспорт.
func WithCustomTransport(transport *http.Client) Option {
	return func(client *client) error {
		if transport == nil {
			return errors.New("client: transport is nil")
		}
		client.transport = transport
		return nil
	}
}

// WithDefaultTransport устанавливает дефолтный транспорт.
func WithDefaultTransport() Option {
	var (
		timeout      = defaultTimeout
		maxIdleConns = defaultMaxIdleConns
	)
	return func(client *client) error {
		if env, present := os.LookupEnv("SERVICE_COIN_MARKET_CAP_TIMEOUT"); present {
			var err error
			if timeout, err = time.ParseDuration(env); err != nil {
				return errors.Wrapf(err, "client: cannot parse timeout environment variable %q", env)
			}
		}
		if env, present := os.LookupEnv("SERVICE_COIN_MARKET_CAP_MAX_IDLE_CONNS"); present {
			var err error
			if maxIdleConns, err = strconv.Atoi(env); err != nil {
				return errors.Wrapf(err, "client: cannot parse idle cons environment variable %q", env)
			}
		}

		transport := &http.Client{
			Timeout: timeout,
			Transport: &http.Transport{
				DialContext: (&net.Dialer{
					Timeout:   2 * time.Second,
					KeepAlive: 10 * time.Second,
				}).DialContext,
				MaxIdleConnsPerHost:   maxIdleConns,
				IdleConnTimeout:       90 * time.Second,
				TLSHandshakeTimeout:   10 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
			},
		}
		return WithCustomTransport(transport)(client)
	}
}
