package converter

import (
	"context"
	"errors"

	"coinconv/internal/services/cointmarketcap"
)

type converter struct {
	coinMarketCapService cointmarketcap.CoinMarketCap
}

func New(service cointmarketcap.CoinMarketCap) *converter {
	return &converter{
		coinMarketCapService: service,
	}
}

func (c *converter) Convert(ctx context.Context, value float64, from, to string) (float64, error) {
	currencyMap, err := c.coinMarketCapService.GetCurrencyMap(ctx)
	if err != nil {
		return 0, err
	}

	_, hasFirst := currencyMap[from]
	_, hasSecond := currencyMap[to]
	if !hasFirst && !hasSecond {
		return 0, errors.New("at least one of the arguments must be a cryptocurrency ")
	}

	isReversed := false
	var symbol, convert string
	if hasFirst {
		symbol = from
		convert = to
	} else {
		symbol = to
		convert = from
		isReversed = true
	}

	convertPrice, err := c.coinMarketCapService.GetCurrencyQuote(ctx, symbol, convert)
	if err != nil {
		return 0, err
	}

	if !isReversed {
		return value * convertPrice, nil
	} else {
		return value / convertPrice, nil
	}
}
