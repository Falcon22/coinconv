module coinconv

go 1.17

require (
	github.com/joho/godotenv v1.4.0
	github.com/kamilsk/retry/v4 v4.8.0
	github.com/pkg/errors v0.9.1
	go.octolab.org v0.12.1
)
