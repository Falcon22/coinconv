package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
	"github.com/pkg/errors"

	"coinconv/internal/services/cointmarketcap"
	coinClient "coinconv/internal/services/cointmarketcap/client"
	"coinconv/internal/services/converter"
)

const (
	envFile = ".env"

	coinMarketCapHostEnv   = "COIN_MARKET_CAP_HOST"
	coinMarketCapApiKeyEnv = "COIN_MARKET_CAP_API_KEY"

	sandboxHost = "https://sandbox-api.coinmarketcap.com"
	sandboxKey  = "sandbox"
)

func main() {
	err := godotenv.Load(envFile)

	if err != nil {
		log.Fatalln("error loading .env file")
	}

	args := os.Args
	if len(args) < 4 {
		log.Fatalln("please, take value, and two currency for converting")
	}

	value, err := strconv.ParseFloat(args[1], 64)
	if err != nil {
		log.Fatalln("first argument must be in numeric format")
	}

	host := os.Getenv(coinMarketCapHostEnv)
	if host == "" {
		host = sandboxHost
	}

	// enter your own apikey or put it in an env vars
	apikey := os.Getenv(coinMarketCapApiKeyEnv)
	if apikey == "" {
		apikey = sandboxKey
	}
	coinMarketCapClient, err := coinClient.New(
		host,
		apikey,
		coinClient.WithDefaultTransport(),
	)
	if err != nil {
		log.Fatalln(errors.Wrap(err, "can't create coinMarketCap"))
	}

	coinMarketCapService := cointmarketcap.New(coinMarketCapClient)
	convert := converter.New(coinMarketCapService)

	result, err := convert.Convert(context.Background(), value, args[2], args[3])
	if err != nil {
		log.Fatalln(errors.Wrap(err, "can't convert currency"))
	}

	fmt.Printf("%f\n", result)
}
